---
title : "Caeté"
description: "Caeté is a html5 multimedia platform that aims to create interactive documents made of any elements supported by a browser, such as: videos, audios, animations and interactive graphs. It can be used to create interactive documentaries, storytelling tools, adding elements to already existing videos and so on."
lead: "Caeté is a html5 multimedia platform that aims to create interactive documents made of any elements supported by a browser, such as: videos, audios, animations and interactive graphs. It can be used to create interactive documentaries, storytelling tools, adding elements to already existing videos and so on."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---
